tables="DW_WELLS
DW_OILFIELD
DW_INCL_REGISTRY
DW_INCLINOMETRY
DW_GIS
DW_DICT_PLAST
DW_DICT_COLLECTOR
DW_DICT_SAT
DW_DICT_LIT
DW_WELL_HEAD_COORDS
DW_WELL_BOREHOLE
DW_MTH_OP_RAP
DW_DICT_CHARWORK
DW_OBJECT
DW_DAILY_OP_MOD
DW_DICT_CHARWORK"

arr=($(echo $tables | tr " " "\n"))


for f in *.sqldb
	do
		dropdb --if-exists ${f%.*}
		createdb ${f%.*}
		psql ${f%.*} < schema.sql
		
		for t in "${arr[@]}"
			do
				sqlite3 $f ".dump ${t}" | grep '^INSERT' > data.sql
				psql ${f%.*} < data.sql
				rm data.sql
		done
		pg_dump -Fc ${f%.*} > "${f%.*}.dump"
done

