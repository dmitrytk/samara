SELECT rates.field                      AS 'Месторождене',
       rates.well                       AS 'Скважина',
       strftime('%d.%m.%Y', rates.date) AS 'Дата',
       rates.work_type                  AS 'Характер работы',
       rates.intake                     AS 'Закачка',
       rates.rate                          'Дебит',
       rates.pressure                   AS 'Давление',
       rates.dynamic_level              AS 'Дин. уровень'
FROM RATES rates
ORDER BY 3 DESC;