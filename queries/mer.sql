SELECT mer.field                      AS 'Месторождение',
       mer.well                       AS 'Скважина',
       mer.work_type                  AS 'Характер работы',
       mer.state                      AS 'Состояние',
       strftime('%d.%m.%Y', mer.date) AS 'Дата',
       mer.plast                      AS 'Пласт',
       mer.production                 AS 'Закачка/Добыча',
       mer.work_days                  AS 'Дней работы',
       mer.upl_debit                  AS 'Упл. дебит'
FROM MER mer;