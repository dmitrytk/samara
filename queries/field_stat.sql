SELECT strftime('%Y', mr.date)           AS 'Год',
       sum(mr.production)                AS 'Закачка за год',
       (SELECT sum(production)
        FROM MER m
        WHERE strftime('%Y', m.date) <= strftime('%Y', mr.date)
          AND work_type = 'Поглощающие') AS 'Накопленная закачка',
       sum(mr.work_days)                 AS 'Скважино дней',
       sum(mr.production) / 365          AS 'Среднесуточная закачка',
       count(DISTINCT mr.well)           AS 'Скважин в работе'
FROM MER AS mr
WHERE mr.work_type = 'Поглощающие'
  AND mr.state = 'В работе'
GROUP BY 1
ORDER BY 1;