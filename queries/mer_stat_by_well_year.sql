SELECT mr.well                 AS 'Скважина',
       strftime('%Y', mr.date) AS 'Год',
       sum(mr.production)      AS 'Закачка',
       (SELECT sum(m.production)
        FROM mer m
        WHERE m.well = mr.well
          AND strftime('%Y', m.date) <= strftime('%Y', mr.date)
          AND work_type = 'Поглощающие'
       )                       AS 'Накопленная закачка',
       sum(mr.work_days)       AS 'Дней работы'
FROM MER mr
WHERE work_type = 'Поглощающие'
GROUP BY 2, 1
ORDER BY 1, 2;