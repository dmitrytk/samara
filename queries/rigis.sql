SELECT fields.OILFIELD_NAME  AS 'Месторождение',
       wells.WELL_NAME       AS 'Скважина',
       plast.VALUE_FULL      AS 'Пласт',
       gis.H                 AS 'Глубина от',
       gis.H + gis.L         AS 'Глубина до',
       gis.H_ABS             AS 'АО от',
       gis.H_ABS + gis.L_ABS AS 'АО до',
       gis.L                 AS 'H',
       gis.L_ABS             AS 'Hабс',
       coll.VALUE_FULL       AS 'Коллектор',
       lit.VALUE_FULL        AS 'Литология',
       sat.VALUE_FULL        AS 'Насыщение',
       gis.K_POR             AS 'Кп',
       gis.K_POR             AS 'Кпр'
FROM DW_OILFIELD fields
         JOIN DW_WELLS wells
              ON wells.OILFIELD_ID = fields.OILFIELD_ID
         JOIN DW_GIS gis
              ON gis.WELL_ID = wells.WELL_ID
         LEFT JOIN DW_DICT_PLAST plast
                   ON plast.ID_CODE = gis.PLAST_ID
         JOIN DW_DICT_COLLECTOR coll
              ON coll.ID_CODE = gis.COLLECTOR
         JOIN DW_DICT_SAT sat
              ON sat.ID_CODE = gis.SAT
         JOIN DW_DICT_LIT lit
              ON lit.ID_CODE = gis.LIT;