SELECT rates.well         AS 'Скважина',
       rates.year         AS 'Год',
       rates.inject       AS 'Закачка',
       rates.pressure     AS 'Давление',
       mer.production     AS 'Закачка за год',
       mer.sum_production AS 'Накопленная закачка',
       mer.work_days      AS 'Дней работы'
FROM MER_WELLS_STAT_VIEW mer
         JOIN RATES_WELLS_STAT_VIEW rates ON mer.well = rates.well
    AND mer.year = rates.year
ORDER BY 1, 2;