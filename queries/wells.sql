SELECT wells.WELL_NAME        AS 'Скважина',
       heads.ALT              AS 'Альтитуда',
       heads.X0               AS 'Х',
       heads.Y0               AS 'У',
       wrk.VALUE_FULL         AS 'Назначение',
       state.VALUE_FULL       AS 'Состояние',
       boreholes.FROM_DR_D    AS 'Дата бурения',
       boreholes.DR_BTM_DPTH  AS 'Пробуренный забой',
       boreholes.CUR_BTM_DPTH AS 'Текущий забой'
FROM DW_WELLS wells
         LEFT JOIN DW_WELL_BOREHOLE boreholes ON
    boreholes.WELL_ID = wells.WELL_ID
         LEFT JOIN DW_WELL_HEAD_COORDS heads ON heads.WELL_ID = wells.WELL_ID
         LEFT JOIN (SELECT * FROM DW_MTH_OP_RAP m WHERE m.DT LIKE '2021/01/01%' GROUP BY WELL_ID) AS mer
                   ON mer.WELL_ID = wells.WELL_ID
         LEFT JOIN DW_DICT_CHARWORK wrk ON wrk.ID_CODE = mer.KCHARWORK
         LEFT JOIN DW_DICT_STATE state ON state.ID_CODE = mer.KSTATE
ORDER BY 1;