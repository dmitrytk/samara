SELECT well        AS 'Скважина',
       date        AS 'Дата',
       max(intake) AS 'Макс. закачка'
FROM RATES
WHERE work_type = 'Поглощающие'
GROUP BY 1;