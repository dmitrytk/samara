SELECT fields.OILFIELD_NAME AS 'Месторождение',
       wells.WELL_NAME      AS 'Скважина',
       inc.DEPTH            AS 'Глубина',
       inc.A_DEPTH          AS 'АО',
       inc.ANGLE            AS 'угол',
       inc.AZIMUTH          AS 'Азимут'
FROM DW_OILFIELD fields
         JOIN DW_WELLS wells ON
    wells.OILFIELD_ID = fields.OILFIELD_ID
         JOIN DW_INCL_REGISTRY increg ON
    increg.WELL_ID = wells.WELL_ID
         JOIN DW_INCLINOMETRY inc ON
    increg.INCL_ID = inc.INCL_ID;