SELECT mer.well            AS 'Скважина',
       SUM(mer.production) AS 'Закачка'
FROM MER mer
WHERE mer.work_type = 'Поглощающие'
GROUP BY 1;

