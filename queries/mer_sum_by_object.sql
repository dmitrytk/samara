SELECT mer.plast           AS 'Пласт',
       sum(mer.production) AS 'Закачка'
FROM MER mer
WHERE mer.work_type = 'Поглощающие'
GROUP BY 1
UNION
SELECT 'Все пласты'        AS plast,
       sum(mer.production) AS production
FROM MER mer
WHERE mer.work_type = 'Поглощающие';