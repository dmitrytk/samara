DROP VIEW IF EXISTS MER_WELLS_STAT_VIEW;
CREATE VIEW MER_WELLS_STAT_VIEW AS
SELECT mr.well                 AS well,
       strftime('%Y', mr.date) AS year,
       sum(mr.production)      AS production,
       (SELECT sum(m.production)
        FROM mer m
        WHERE m.well = mr.well
          AND strftime('%Y', m.date) <= strftime('%Y', mr.date)
          AND work_type = 'Поглощающие'
       )                       AS sum_production,
       sum(mr.work_days)       AS work_days
FROM MER mr
WHERE work_type = 'Поглощающие'
GROUP BY 2, 1
ORDER BY 1, 2;

DROP VIEW IF EXISTS RATES_WELLS_STAT_VIEW;
CREATE VIEW RATES_WELLS_STAT_VIEW AS
SELECT well                 AS well,
       strftime('%Y', date) AS year,
       CASE
           WHEN min(intake) = max(intake) THEN avg(intake)
           ELSE min(intake) || ' - ' || max(intake)
           END              AS inject,
       CASE
           WHEN min(pressure) = max(pressure) THEN avg(pressure)
           ELSE min(pressure) || ' - ' || max(pressure)
           END              AS pressure
FROM RATES
WHERE (work_type = 'Поглощающие'
    AND intake > 0)
   OR (work_type = 'Поглощающие' AND pressure > 0)
GROUP BY 1, 2;