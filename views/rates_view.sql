DROP VIEW IF EXISTS RATES;
CREATE VIEW RATES AS
SELECT fields.OILFIELD_NAME                                   AS field,
       wells.WELL_NAME                                        AS well,
       DATE(SUBSTR(rates.MEASURED_IN, 1, 4)
                || '-' || SUBSTR(rates.MEASURED_IN, 6, 2)
                || '-' || SUBSTR(rates.MEASURED_IN, 9, 2)) AS date,
       wrk.VALUE_FULL                                         AS work_type,
       rates.INTAKE                                           AS intake,
       rates.LIQ_RATE                                            rate,
       rates.CASING_PRESSURE                                  AS pressure,
       rates.H_DYN                                            AS dynamic_level
FROM DW_OILFIELD fields
         JOIN DW_WELLS wells ON
    wells.OILFIELD_ID = fields.OILFIELD_ID
         JOIN DW_DAILY_OP_MOD rates ON
    rates.WELL_ID = wells.WELL_ID
         JOIN DW_DICT_CHARWORK wrk ON
    wrk.ID_CODE = rates.KCHARWORK
WHERE wrk.VALUE_FULL IN ('Поглощающие', 'Водозаборные')
ORDER BY 3 DESC;