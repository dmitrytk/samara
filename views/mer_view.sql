DROP VIEW IF EXISTS MER;
CREATE VIEW MER AS
SELECT field.OILFIELD_NAME                    AS field,
       wells.WELL_NAME                        AS well,
       st.VALUE_FULL                          AS state,
       DATE(SUBSTR(mer.DT, 1, 4)
           || '-' || SUBSTR(mer.DT, 6, 2)
           || '-' || SUBSTR(mer.DT, 9, 2)) AS date,
       obj.OBJECT_NAME                        AS plast,
       mer.WAT_LIQ_INJ_M3                     AS production,
       mer.WORKTIME / 24                      AS work_days,
       mer.WAT_LIQ_INJ_M3 / mer.WORKTIME * 24 AS upl_debit,
       wrk.VALUE_FULL                         AS work_type
FROM DW_MTH_OP_RAP mer
         JOIN DW_WELLS wells ON
    wells.WELL_ID = mer.WELL_ID
         JOIN DW_OILFIELD field ON
    wells.OILFIELD_ID = field.OILFIELD_ID
         JOIN DW_DICT_CHARWORK wrk ON
    mer.KCHARWORK = wrk.ID_CODE
         JOIN DW_OBJECT obj ON
    obj.OBJECT_ID = mer.OBJECT_ID
         JOIN DW_DICT_STATE st ON
    st.ID_CODE = mer.KSTATE
WHERE wrk.VALUE_FULL IN ('Поглощающие', 'Водозаборные')
ORDER BY 2 DESC;