import os
import shutil
import sqlite3
import pandas as pd


db_path = os.path.join(os.environ['USERPROFILE'], 'РАБОТА/ПРОЕКТЫ/БАЗЫ/')

dbs = [file for file in os.listdir(db_path) if '.sqldb' in file]
views = [file for file in os.listdir('views')]
queries = {
    'field_stat': 'Статистика по месторождению',
    'inclinometry': 'Инклинометрия',
    'mer_stat_by_well_year': 'МЭР по скважинам и годам',
    'mer_sum_by_object': 'Сумма по пластам',
    'mer_sum_by_wells': 'МЭР сумма по скважинам',
    'rates_max': 'Макс дебиты',
    'rigis': 'РИГИС',
    'well_stat': 'Статистика по скважинам',
    'wells': 'Скважины',
    'rates': 'Режимы',
    'mer': 'МЭР',
}


def read_query_from_file(file_name: str) -> str:
    with open(file_name, 'r', encoding='utf8') as file:
        return file.read()


 # Create statistics folder
stat_folder = 'Статистика'
if os.path.exists(stat_folder):
    shutil.rmtree(stat_folder)
os.mkdir(stat_folder)

for db in dbs:
    db_name = db.split('.')[0]
    print(f'Processing {db_name}')

    # Create db folder
    dest_folder = f'{stat_folder}/{db_name}'
    if os.path.exists(dest_folder):
        shutil.rmtree(dest_folder)
    os.mkdir(dest_folder)

    # Create DB views
    for view in views:
        os.system(f'sqlite3 {db_path}/{db} < views/{view}')

    # Connect to DB
    conn = sqlite3.connect(f'{db_path}/{db}')
    for query in queries.keys():
        # query_name = query.split('.')[0]
        df = pd.read_sql(read_query_from_file(f'queries/{query}.sql'), conn)
        # Export to csv
        df.to_csv(f'{dest_folder}/{queries[query]}.csv',
                  encoding='1251', sep=';', index=False)
